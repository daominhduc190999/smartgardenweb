var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
// var logger = require('morgan');
const expressLayouts = require('express-ejs-layouts');
const upload = require('express-fileupload')

var indexRouter = require('./routes/index');
var productsRouter = require('./routes/products');
var profilesRouter = require ('./routes/profile')
var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.use(expressLayouts);
app.set('view engine', 'ejs');
app.set('layout','mainLayout');
app.set("layout extractScripts", true);
app.set("layout extractStyles", true);


// app.use(logger('dev'));
app.use(upload())
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/public',express.static(path.join(__dirname, 'public')));


app.use('/', indexRouter);
app.use('/products', productsRouter);
app.use('/profile',profilesRouter)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
