var express = require('express');
var router = express.Router();
let listItem = require('../API/listItemProducts')
let listItemHome = require ('../API/listItemHomeProducts');
const { default: fetch } = require('node-fetch');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { listItemHome:listItemHome, title:'Trang Chủ'});
});
/* GET list products page. */
router.get('/products', function(req, res, next) {
  res.render('products',{listItem:listItem,title:'Sản Phẩm'});
});
/* GET login page. */
router.get('/login', function(req, res, next) {
  res.render('login',{title:'Đăng Nhập'});
});
/* GET sign up page. */
router.get('/sign-up', function(req, res, next) {
  res.render('signup',{title:'Đăng Ký'});
});
/* GET profile page. */
router.post('/profile', function(req, res, next) {
  let {username, password} = req.body
  res.render('profile',{username,title:username})
});


module.exports = router;
